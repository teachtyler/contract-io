## Development

### Prerequisites 
Have Node.js Installed


```bash
#install node modules
yarn
#start dev server
npm start
```

The server will load on :3000 and watch for scss/ts changes.


