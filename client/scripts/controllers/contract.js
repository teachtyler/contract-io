(function() {
	'use strict';

	angular
		.module('app')
		.controller('ContractController', ContractController);

	ContractController.inject = ['$scope', '$routeParams', '$location', 'contractService'];

	function ContractController($scope, $routeParams, $location, contractService) {
		var vm = this;

		if ($routeParams.id) {
			contractService.getContract($routeParams.id)
				.then(res => {
					vm.contract = res.data
				})
				.catch(err => {
					alert(err)
					$location.path('/')
				})
		}

		vm.signed = function(sig) {
			vm.contract.owner.sig = sig;

			contractService.updateContract(vm.contract)
				.then()
				.catch(err => console.log(err))
		}

		vm.editLink = function() {
			$location.path(`/${$routeParams.id}`)
		}

	}
})();
