(function() {
	'use strict';

	angular
		.module('app')
		.controller('MainController', MainController);

	MainController.$inject = ['$scope', '$routeParams', '$location', 'contractService'];

	function MainController($scope, $routeParams, $location, contractService) {
		const vm = this;
		const date = new Date();
		const contractTemplate = {
			title: "Title",
			day: date.getDate(),
			month: date.getMonth() + 1,
			year: date.getFullYear(),
			contractor: {
				name: "",
				sig: {},
				date: date
			},
			owner: {
				name: "",
				sig: {},
				date: date
			},
			state: "",
		}

		if ($routeParams.id === undefined) {
			vm.contract = contractTemplate
		} else {
			// $location.path(`/contract/${$routeParams.id}`)
			contractService.getContract($routeParams.id)
				.then(res => {
					debugger;

					console.log(res)
					if (res.success !== undefined && !res.success) {
						throw new Error(res.message)
					}
					vm.contract = res.data
				})
				.catch(err => {
					debugger;
					vm.contract = contractTemplate
					console.log(err)
				})
		}

		vm.addSection = function() {
			vm.contract.sections = vm.contract.sections || []
			let newSection = {
				id: vm.contract.sections.length + 1,
				title: "Title",
				overview: "overview",
				description: "description"
			}
			vm.contract.sections.push(newSection)
		}

		vm.removeSection = function() {
			// todo remove by ID
			vm.contract.sections.pop();
		}

		vm.signed = function(sig) {
			vm.contract.contractor.sig = sig;
			vm.updateContract()
		}

		vm.updateContract = function() {
			contractService.updateContract(vm.contract)
		}

		vm.createLink = function() {
			if ($routeParams.id) {
				$location.path(`/contract/${$routeParams.id}`)
			} else {
				contractService.createContract(vm.contract)
					.then((res) => {
						if (res.status !== 200 || res.data._id === undefined) {
							throw new Error('Failed to create')
						} else {
							$location.path('/contract/' + res.data._id)
						}
					})
					.catch(err => console.log(err))
			}
		}

	}
})();
