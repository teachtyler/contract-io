(function() {
	'use strict';

	angular
		.module('app')
		.factory('contractService', contractService);

	contractService.$inject = ['$http'];

	function contractService($http) {
		var api = {};
		const baseUrl = `${location.origin}/api/contract`

		// Methods
		function createContract(contract) {
			return $http.post(`${baseUrl}/create`, contract).then(handleSuccess, handleError('Error creating user'));
		}

		function getContract(id) {
			return $http.get(`${baseUrl}/${id}`).then(handleSuccess, handleError('Error getting user'));
		}

		function updateContract(contract) {
			return $http.put(`${baseUrl}/${contract.id}`, contract).then(handleSuccess, handleError('Error getting user'));
		}

		function deleteContract(id) {
			return $http.delete(`${baseUrl}/${id}`, contract).then(handleSuccess, handleError('Error getting user'));
		}

		// Handlers
		function handleSuccess(res) {
			return res;
		}

		function handleError(error) {
			return function() {
				return { success: false, message: error };
			};
		}

		// api.create = createContract
		// api.get = getContract
		// api.update = updateContract
		// api.delete = deleteContract

		// Export
		return  {
			createContract,
			getContract,
			updateContract,
			deleteContract
		};
		////////////////

	}
})();
