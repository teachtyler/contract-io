import { Routes } from './index';
import { Router } from 'express'

module.exports = (app: Router) => {

    const { Contract } = Routes;
    const contract = new Contract()

    app.post('/api/contract/create', contract.create)
    app.get('/api/contract/:id', contract.get)
    app.put('/api/contract/:id', contract.update)
    app.delete('/api/contract/:id', contract.delete)


}
