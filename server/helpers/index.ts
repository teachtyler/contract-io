export const handleResponse = (res) => {
    return (result, err) => {
        if (err)
            res.send(404, err)

        res.send(result)
    }
}
