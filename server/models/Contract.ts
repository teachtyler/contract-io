import { Schema, Model, model } from "mongoose";
import IContract from './IContract'

const ContractSchema: Schema = new Schema({
	id: Number,
	title: String,
	day: Number,
	month: Number,
	year: Number,
	sections: [
		{
			id: Number,
			title: String,
			overview: String,
			description: String
		}
	],
	contractor: {
		name: String,
		sig: Object,
		date: Date
	},
	owner: {
		name: String,
		sig: Object,
		date: Date
	},
	state: String
})

const ContractModel: Model<IContract> = model<IContract>("Contract", ContractSchema)

export class Contract {
	constructor() { }

	create(contract: IContract, cb: (...args) => void) {
		const newContract = new ContractModel({ ...contract })

		newContract
			.save()
			.then(res => cb(res))
			.catch(err => cb(null, err))
	}

	get(id: String, cb: (...args) => void) {
		ContractModel.findById(id)
			.then(res => cb(res))
			.catch(err => cb(null, err))
	}

	update(contract: IContract, cb: (...args) => void) {
		const { _id } = contract

		ContractModel.findOneAndUpdate({ _id }, contract, { upsert: true })
			.then(res => cb(res))
			.catch(err => cb(null, err))
	}

	delete(id: Number, cb: (...args) => void) {
		ContractModel.findOneAndRemove({ id })
			.then(res => cb(res))
			.catch(err => cb(null, err))
	}
}
