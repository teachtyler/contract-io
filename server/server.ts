const path = require('path'),
	express = require('express'),
	app = express(),
	mongo = require('mongoose'),
	bodyParser = require('body-parser');

require('dotenv').config()

mongo.connect(process.env.MONGO_DB)

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

app.use(express.static(path.resolve(__dirname, '../client')));

require('./routes/router')(app)

const server = app.listen(process.env.port || 3000, () => {
	console.log('Listening on port %s...', server.address().port);
});
